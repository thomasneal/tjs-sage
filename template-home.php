
<?php
/**
 * Template Name: Home
 */
 use Roots\Sage\Extras;
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php #get_template_part('templates/content', 'page'); ?>
  <main>
      <section id="jumbotron" class="full">
          <div class="jumbotron">
              <div class="jumbotron-inner">
                  <h1 class="display-4"><?php Extras\acf_field("jumbotron_title"); ?></h1>
                  <p class="lead"><?php Extras\acf_field("jumbotron_hero"); ?></p>
                  <hr class="my-2">
                  <p><?php Extras\acf_field("jumbotron_text"); ?></p>
              </div>
              <p class="lead">
                <a class="btn btn-primary btn-lg" href="
                <?php Extras\acf_field("jumbotron_button_link"); ?>" role="button">
                Learn more</a>
              </p>
          </div>
    </section>
      <section id="emphasis-container">
          <div class="row">
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_1_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_1_text"); ?>
              </div>
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_2_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_2_text"); ?>
              </div>
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_3_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_3_text"); ?>
              </div>
          </div>
          <div class="row">
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_4_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_4_text"); ?>
              </div>
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_5_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_5_text"); ?>
              </div>
              <div class="emphasis col-md-4">
                <h3><?php Extras\acf_field("emphasis_group_6_title"); ?></h3>
                <?php Extras\acf_field("emphasis_group_6_text"); ?>
              </div>
          </div>
      </section>
  </main>
<?php endwhile; ?>
