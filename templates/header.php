<header class="banner">
  <div class="container">
    <nav class="navbar navbar-light navbar-fixed-top">
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
          <span class="logo"><img src="<?php echo get_template_directory_uri() . '/assets/images/logo.png'; ?>"
              width="30" height="30" class="d-inline-block align-top" alt=""/>
          </span>
          <?php bloginfo('name'); ?>
      </a>
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>
  </div>
</header>
